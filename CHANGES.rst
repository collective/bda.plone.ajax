Changes
=======

1.6 (unreleased)
----------------

- Disable diazo theming in ``ajaxaction`` and ``ajaxform`` browser views.
  [rnix, 2014-12-10]

- Add ``AjaxPath`` continuation. Can be used as of ``bdajax`` 1.6.
  [rnix, 2014-12-10]


1.5
---

- Add ajaxform convenience browser page.
  [rnix, 2014-02-04]


1.4
---

- Cleanup docs.
  [rnix, 2013-10-21]

- Do not load examples by default.
  [rnix, 2013-10-21]

- Add abstract batch for buidling ajax batches.
  [rnix, 2013-10-20]


1.3
---

- Provide overlay configuration.
  [rnix, 2012-08-06]

- Provide form continuation.
  [rnix, 2012-08-06]


1.2.2
-----

- render viewlet in IPortalTop, so it pops up centered and not at the end of
  the site.
  [jensens, 2011-12-02]

- add z3c.autoinclude entry point.
  [jensens, 2011-12-02]


1.2.1
-----

- display ``bdajax.message`` with traceback if ``ajaxaction`` throws uncaught
  exception.
  [rnix]


1.2
---

- add ajax continuation support and continuation helper objects and functions.
  [rnix]


1.1
---

- add examples.
  [rnix]

- add ajaxaction view.
  [rnix]


1.0
---

- make it work.
  [rnix]
