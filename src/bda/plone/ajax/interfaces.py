from zope.interface import Interface


class IAjaxLayer(Interface):
    """Browserlayer for ``bda.plone.ajax``
    """
